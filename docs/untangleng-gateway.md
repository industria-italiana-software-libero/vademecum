# UntangleNG Gateway

**Sito web: **[https://www.untangle.com/untangle-ng-firewall/](https://www.untangle.com/untangle-ng-firewall/ "Sito Ufficiale UntangleNG Gateway")

**Funzionalità principali:**

Gateway-firewall basato su Debian con funzioni aggiuntive di spamblocker, antivirus, filtro web, router, intrusion prevention e phishing blocker; modulare e flessibile, consente l'installazione di numerosi plug-in per incrementare le funzionalità. Usabilità garantita dal pratico ambiente grafico Xfce.

**Vantaggi per le aziende:**

Soluzione di sicurezza IT facile da implementare, valida e personalizzabile. anche in contesti strutturati.

**Esempio pratico di utilizzo:**

Creazione di una soluzione di sicurezza molto personalizzabile tramite plug-in e di facile utilizzo.

**Licenza:** GPL v.2.0

