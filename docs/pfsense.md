# PfSense

**Sito web: **[https://www.pfsense.org/](https://www.pfsense.org/ "Sito Ufficiale PfSense")

**Funzionalità principali:**

firewall di tipo Packet Filter che fornisce sicurezza e funzionalità ad ampio spettro come la creazione di PPPoE server, VPN server su protocollo IPsec e OpenVPN, uPnP, DNS dinamici, load balancing dei servers dietro al firewall o comunque presenti in una DMZ, high availability \(HA\),supporto NAT, gestione multi-WAN in bilanciamento di carico o fault tolerance.

**Vantaggi per le aziende:**

Capacità di creare strutture di rete affidabili, ad alta efficienza e resilienza.

**Esempio pratico di utilizzo:**

Realizzazione di router con funzioni di networking avanzate con strumenti per il controllo del traffico di rete.

**Licenza:** BSD

