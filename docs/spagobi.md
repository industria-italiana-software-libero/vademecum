# SpagoBI

**Sito web: **[https://www.spagobi.org/](https://www.spagobi.org/ "Sito Ufficiale Spago BI")

**Funzionalità principali:**

Suite per la BI \(Business Intelligence\) che supporta processi decisionali di business tattici e strategici a livello operativo e teorico, coprendo con tutti i suoi strumenti tutte le aree d'interesse della BI quali il data mining, dashboarding, report e modelli di sviluppo ed interazione dati.

**Vantaggi per le aziende:**

Ottimizzazione dei processi strategici e produttivi, analisi dati accurate.

**Esempio pratico di utilizzo:**

Sviluppo di un modello di Business attraverso gli strumenti di previsione, calcolo e dashboarding.

**Licenza:** MPL v.2.0

