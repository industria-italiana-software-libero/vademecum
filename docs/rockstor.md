# Rockstor

**Sito web: **[http://rockstor.com/](http://rockstor.com/)

**Funzionalità principali:**

Sistema Operativo basato su Linux / CentOS dedicato alla creazione di NAS enterprise e soluzioni cloud private, “on-premise”.

**Vantaggi per le aziende:**

Facilità di configurazione ed utilizzo tramite l'apposita interfaccia fruibile tramite browser, alto grado di personalizzazione e modularità dell'unità di storage; supporta la gestione dei volumi on-line, CoW Snapshot, replica asincrona e compressione, il file sharing via NFS, Samba, SFTP, AFP.

**Esempio pratico di utilizzo:**

** **Creazione di un NAS per backup centralizzato in una rete LAN di piccole e medie dimensioni.

**Licenza:** GPL v.2

