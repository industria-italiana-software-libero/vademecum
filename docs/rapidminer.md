# RapidMiner

**Sito web: **[https://rapidminer.com/](https://rapidminer.com/ "Sito ufficiale Rapid Miner")

**Funzionalità principali:**

Software di Business Intelligence che offre un ambiente integrato per funzioni di machine learning, data mining, text mining, analisi predittive aziendali, commerciali, ricerca, prototipazione rapida; supporta tutte le fasi del data mining.

**Vantaggi per le aziende:**

Incremento dell'efficienza del proprio business con l'utilizzo di nuovi modelli di modellazione dati.

**Esempio pratico di utilizzo:**

Analisi di grandi quantità di dati per operazioni di elaborazione di risultati complessi utili alle strategie d'intervento e approccio in campo scientifico e aziendale.

**Licenza:** AGPL v.3

