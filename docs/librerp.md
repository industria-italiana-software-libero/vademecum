# LibrERP

**Sito web: **[http://www.librerp.com/](http://www.librerp.com/ "Sito web ufficiale LibrERP")

**Funzionalità principali:**

Software per la gestione aziendale integrata in cloud computing delle Piccole e medie imprese Italiane.

**Vantaggi per le aziende:**

ERP con localizzazione italiana delle funzionalità, include la fatturazione elettronica.

**Esempio pratico di utilizzo:**

Gestione del ciclo dell'attivo dell'azienda, dal Customer Relationship Management alla fatturazione, alla movimentazione del magazzino, all'assistenza post - vendita.

**Licenza:** AGPL v.3

