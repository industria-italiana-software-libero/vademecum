# ProxmoxVE

**Sito web: **[https://www.proxmox.com/en/](https://www.proxmox.com/en/ "Sito web ufficiale proxmoxVE")

**Funzionalità principali:**

Hypervisor di livello 1 KVM-based che permette la gestione di macchine virtuali e container \(LXC\), permettendo di virtualizzare storage, reti e cluster HA.

**Vantaggi per le aziende:**

Utilizzo efficiente delle risorse hardware disponibili alle macchine virtuali, possibilità di migrazioni in modalità “live”, web- monitoring e gestione semplificate dei volumi di storage, cluster, interfacce e dispositivi di rete.

**Esempio pratico di utilizzo:**

Realizzazione e gestione di uno o più gruppi di macchine virtuali capaci di lavorare in alta affidabilità condividendo le risorse fisiche con facilità e trasparenza.

**Licenza:** AGPL v.3

