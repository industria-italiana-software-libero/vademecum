# Wordpress

**Sito web: **[https://it.wordpress.org](https://it.wordpress.org "Sito web italiano wordpress")

**Funzionalità principali:** 

Piattaforma per l’esposizione online di contenuti quali blog aziendali, portali descrittivi di prodotti e servizi offerti, moduli di contatto per i clienti.

**Vantaggi per le aziende:** 

Immediatezza di utilizzo, rapidità della curva di apprendimento per l’utente.

**Esempio pratico di utilizzo:**

** **Blog aziendale con la descrizione dei prodotti in vendita ed esempi di loro utilizzo.

**Licenza:** GPL.

