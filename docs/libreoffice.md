# LibreOffice

**Sito web: **[https://it.libreoffice.org/](https://it.libreoffice.org/ "Sito ufficiale italiano LibreOffice")

**Funzionalità principali:**

Software per la produttività personale in ufficio, include videoscrittura, foglio di calcolo, presentazioni e database.

**Vantaggi per le aziende:**

Gestione nativa il formato OpenDocument, uno standard di salvataggio dei documenti che permette la conservazione degli stessi nel tempo.

**Esempio pratico di utilizzo:**

Creazione veloce di una presentazione aziendale per un intervento ad una conferenza.

**Licenza:** LGPL

