.. Vademecum documentation master file, created by
   sphinx-quickstart on Wed Dec 27 12:07:53 2017.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Vademecum sul software libero in azienda
=====================================

.. toctree::
   :maxdepth: 2

   introduzione
   libreoffice
   wordpress
   logicaldoc
   birt
   dariks-boot-and-nuke
   ghost
   jitsi
   librerp
   odoo
   logicaldoc
   nethserver
   opnsense
   penthao
   pfsense
   planner
   porteuskiosk
   proxmoxve
   rapidminer
   rockstor
   roundcube
   spagobi
   untangleng-gateway
   woocommerce


   Indices and tables
   ==================

   * :ref:`genindex`
   * :ref:`search`
