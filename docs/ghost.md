# Ghost

**Sito web: **[http://www.ghost.org/](http://www.ghost.org/ "Sito ufficiale Ghost")

**Funzionalità principali:**

Ghost è una pratica piattaforma per i blogger scritta in Javascript. Permette di gestire un blog aziendale senza avere grosse conoscenze di carattere tecnico.

**Vantaggi per le aziende:**

Molto semplice e facile da gestire, permette di creare un blog aziendale in poco tempo e senza grosse conoscenze di carattere tecnico.

**Esempio pratico di utilizzo:**

Blog aziendale, siti di news e giornalismo online.

**Licenza: **MIT License

