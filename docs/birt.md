# BIRT

**Sito web: **[http://www.eclipse.org/birt/](http://www.eclipse.org/birt/ "Sito Ufficiale BIRT")

**Funzionalità principali:**

Data reporting e BI con funzione di analisi e modellazione dati, permettendo di creare report in modo facile e lineare.

**Vantaggi per le aziende:**

Miglioramento della gestione e della finalizzazione dei processi dianalisi dei dati anche online con approccio OLAP \(On-Line Analytical Processing\).

**Esempio pratico di utilizzo:**

Razionalizzazione dell'analisi e della gestione di dati, al fine di convertirli in risultati utilizzabili in modo pragmatico per attuazione di Business Plan e piani d'azione che necessitano previsioni.

**Licenza:** EPL v.1.0

