# Vademecum sul software libero in azienda.

Questo repository raccoglie i sorgenti del Vademecum sul Software Libero in azienda.

Nella cartella principale del repository sono presenti i file del vademecum in formato open document e pdf.

Nella cartella "docs" sono presenti i sorgenti che alimentano la [pagina read the docs](http://vademecum.readthedocs.io/en/latest/). 
